function guardarEnLocalStorage() {
  var txtClave = document.getElementById("txtClave");
  var txtValor = document.getElementById("txtValor");
  var clave = txtClave.value;
  var valor = txtValor.value;
  localStorage.setItem(clave,valor);
}
function leerDeLocalStorage() {
  var txtClave =document.getElementById("txtClave");
  var clave = txtClave.value;
  var valor = localStorage.getItem(clave);
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = valor;
}
